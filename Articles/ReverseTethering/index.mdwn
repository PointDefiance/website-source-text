I currently run Paranoid Android on my [SG5][1], which works well enough for my
tastes. One thing I've wanted to play with is reverse tethering.

## Rationale

I typically charge my player via my netbook's usb connection, which can be a
bit slow. When wifi is enabled on it, it's doubly slow. A reverse-tether would
allow the player to utilize the internet connection on the netbook through the
usb cable instead of using the built in power-draining wifi radio.

I also have a few apps like calibre and the [companion app][2] which don't work
correctly when I'm on a public wifi. Typically these wifi networks have been
been firewalled to prevent devices from communicating with each other. A
reverse-tether bypasses the wifi network and gives me a nice secure connection
between both devices.

I got it working ... mostly. I don't expect many people to find this useful,
but I thought it might be an entertaining hack to poke at. It requires a linux
computer as the host, and a android phone with the [android ports
collection][3] installed along with dropbear (an ssh server for android).

## The Scripts

The following script sits on my linux netbooks' `~/bin` directory as `reverse-tether.sh`

    #!/bin/sh

    # We need root on the host to mess with networking
    if \[[ $(whoami) != "root" ]]; then
      echo "You must be root to run this script!"
      exit 1
    fi;

    start() {

        # Check for a player device already plumbed
        if [ `ifconfig -a | grep "player" | cut -d ':' -f 1` ]; then
            echo "Device is already up..."
            exit 1
        fi

        # We need root on the device to mess with networking
        echo 'Enabling adb root access...'
        adb -d root

        sleep 4
        echo 'Waiting for device to settle...'

        # Turn on usb networking
        echo 'Enabling usb network interface on the device... (via remote adb shell script)'
        adb -d shell 'sh /storage/sdcard0/reverse_tether.sh up &'
        
        echo 'Waiting for device to settle...'
        sleep 4

        # Rename device, annoying really (becomes player)
        IF=`ifconfig -a | grep "enp" | cut -d ':' -f 1`

        if [ -n $IF ]; then
            ip link set dev $IF down
            /usr/sbin/ifrename -i $IF -n player
            ip link set dev player up
            echo "Renamed wierd device name to player device..."
        else
            echo "Device rename failed, try again..."
            exit 1
        fi;

        echo 'Setting Local IP Stuff ...'
        ifconfig player 192.168.200.1 netmask 255.255.255.0
        ifconfig player up
        iptables -F -t nat
        iptables -A POSTROUTING -t nat -j MASQUERADE
        echo 1 > /proc/sys/net/ipv4/ip_forward

        echo 'Starting dnsmasq...'
        dnsmasq --interface=player --no-dhcp-interface=player
    }

    stop () {

        echo 'Using ssh to stop tethering on android device...'
        ssh root@192.168.200.2 'sh /storage/sdcard0/reverse_tether.sh down &'

        echo 'Shutting down reverse tethering locally...'

        killall dnsmasq
        ifconfig player down
        iptables -F -t nat
        echo 0 > /proc/sys/net/ipv4/ip_forward

    }


    case $1 in
            start)
                    start
            ;;
            stop)
                    stop
            ;;
            restart)
                    start
                    stop
            ;;
            *)
                    echo "Usage start|stop|restart"
    esac

And the following script is simply stashed on the sdcard of the player. as `/storage/sdcard0/reverse_tether.sh`

    #!/system/bin/sh

    if [ "$1" = "up" ]; then
        echo 1 > /sys/class/usb_composite/rndis/enable
     
        ip addr add 192.168.200.2/24 dev usb0
        ip link set usb0 up
        ip route delete default
        ip route add default via 192.168.200.1
        setprop net.dns1 192.168.200.1
        /data/local/bin/dropbear
    fi

    if [ "$1" = "down" ]; then
        sleep 2
        ip route delete default
        ip link set usb0 down
        killall dropbear
        echo 0 > /sys/class/usb_composite/rndis/enable
    fi

When I finish, sometimes I need to restart wifi on the player to get networking to work again.

## How it works

The script is started on the netbook running linux. It fires up adb (the
Android Debug Bridge) daemon, and connects to the player and tells the player
to execute the script loaded on the sdcard. It would be great if I could use
the adb connection and a virtual network up concurrently, but as the adb bridge
drops as soon as the rndis virtual network is enabled, I needed a way to bring
up the network on the android device up and configured when the connection is
lost.

While the connection is brought up, the linux script sleeps a few seconds, then
renames the interface to something reasonable. Finally, it assigns an ip and
enables NAT and waits. While the network is active, adb is pretty much useless
so any remote control of the android needs to be done via ssh instead of with
adb. Hence when you continue the script, the first thing it does is login to
the player via ssh and executes the second half of the remote script to bring
down the network, clear the interfaces, and disable rndis. Adb should be
available again at this point as well.

## Summary

The only real problem I've had is that some apps require or want wifi/3g active
in order to fully function. For example, the play store works fine until you
try and download an actual update. Google Maps works fine, although you get
lots of warnings about having wifi turned off. I have no idea how to "emulate"
3g or wifi which I think would solve that problem.

[1]: http://www.samsung.com/us/mobile/mp3-players/YP-G70CWY/XAA 
    "Samsung Galaxy Player 5"
[2]: https://play.google.com/store/apps/details?id=com.multipie.calibreandroid&hl=en 
    "Calibre Companion"
[3]: http://dan.drown.org/android/

[[!meta updated="Tue Jun  4 15:06:45 PDT 2013"]]
[[!meta date="Wed Apr 05 09:19:00 PDT 2013"]]
[[!meta title="Reverse Tethering"]]
[[!meta author="Defiance"]]
[[!meta authorurl="http://point-defiance.rhcloud.com/Authors/Defiance"]]
[[!meta license="This work is [[free|PrimarilyPublicDomain]] of known copyright restrictions."]]
[[!meta copyright="&copy; 2013 by Defiance"]]
[[!tag Article Android Linux Geekery Howto Scripting Tips]]
[[!inline pages="Articles/CommonReferences" feeds="no" raw="yes"]]
