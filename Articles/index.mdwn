These are longer "blog" posts, and essays.  If your looking for a more
traditional blog content, try [[Muse]].

[[!inline 
   pages="Articles/* and !Articles/CommonReferences and !*.png and !*.jpg and !*/pingback* and !*/refmeta* and !*/nearlink*" 
   description="Point Defiance - Articles"
   archive=yes
   feeds=no
   template="articlelist"
]]

[[!trailitems pages="Articles/* and !Articles/CommonReferences and !*.png and !*.jpg and !*/pingback* and !*/refmeta* and !*/nearlink*" sort="age"]]
[[!trailoptions  sort="meta(title)" circular="no"]]

[[!meta date="April 05, 2013"]]
[[!meta title="Articles of Interest"]]
[[!meta author="Defiance"]]
[[!meta authorurl="http://point-defiance.rhcloud.com/Authors/Defiance"]]
[[!meta license="This work is [[free|PrimarilyPublicDomain]] of known copyright restrictions."]]
[[!meta copyright="&copy; 2013 by Defiance"]]
[[!inline pages="/SiteReferences" feeds="no" raw="yes"]]

