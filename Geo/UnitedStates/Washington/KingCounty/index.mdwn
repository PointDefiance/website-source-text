## Tagged with "King County"

<ul>[[!inline
   pages="tagged(KingCounty)"
   archive=yes
   feeds=no
   feedshow=0
   template="geolist"
]]</ul>

## Tagged with locations within "King County"

<ul>[[!inline
   pages="
      tagged(Seattle)
   or tagged(Auburn)"
   archive=yes
   feeds=no
   feedshow=0
   template="geolist"
]]</ul>

[[!trailitems pagenames="
Geo/UnitedStates/Washington/KingCounty/Seattle
Geo/UnitedStates/Washington/KingCounty/Auburn
"]]
[[!meta title="Geographical Locations: King County, WA"]]
[[!meta author="Defiance"]]
[[!meta authorurl="http://point-defiance.rhcloud.com/Authors/Defiance"]]
[[!meta license="This work is [[free|PrimarilyPublicDomain]] of known copyright restrictions."]]
[[!meta copyright="&copy; 2013 by Defiance"]]
[[!tag KingCounty WashingtonState UnitedStates]]

-------------------------------------------------------------------------------

