## What is Feminism?

> It's a great question. A feminist is someone who subscribes to feminist
> theory.  Feminist theory has an enormous number of variations, (liberal,
> socialist, sex-positive, radical, womanist, etc.) but they all subscribe to
> three fundamental theoretical pillars: rape culture, patriarchy, and
> de-essentialism.
>
> De-essentialism, as it relates to feminism and gender, is the theory that
> there are no intrinsic differences between men and women. If this is true,
> then the enormous gender imbalances present in say, upper level management,
> must be the result of some outside factor.
>
> Enter Patriarchy theory. Patriarchy theory presumes that, throughout western
> history, men have been involved in a conspiracy to oppress women.  They
> accomplish this aim through sexual terrorism, aka rape culture.
> [[RapeCulture]] assumes that rape in western societies is widespread,
> acceptable and even condoned.
>
> So there you have it. That's modern feminism in a (small) nutshell.
> 
> <footer>-- *"[The GOP and the Manosphere](#refs)"* (Red Pill Theory, 2013)</footer>


## Motivations

See this [reddit post](http://www.reddit.com/r/MensRights/comments/16iqlc/im_actually_offended_and_ashamed_that_youre/c7wgn02).

## How it works

There are a few websites in the [[Manosphere]] which are actively
anti-feminist, but for the most part the stated goals of feminism - an end to
prescriptive gender roles, legal and social equality, an end to sexual
discrimination, etc... not many disagree with.

The way feminism goes about achieving those goals, however, can be problematic.

* Typecasting men as oppressive patriarchs, and typecasting women as
  agency-free perpetual victims - gender roles as bad and as strongly imposed as
  any throughout history.

* Characterizing male sexuality as exploitative, predatory, disgusting and zero-sum.

* Characterizing violence against and abuse of men as trivial (or just plain
  hilarious) when compared to violence against women, denying victim status to
  men, and where it is actually acknowledged, going on to blame them for it.

* Using 'privileged' and 'patriarchy' to denote men as out-group, a class whose
  welfare is unimportant and to whom

* fairness does not apply.

* Characterizing men as 'potential rapists' and sexual-predators-in-waiting,
  opening the door to astounding bigotry.

* Allowing and encouraging a culture of bigotry and fear to drive male
  teachers, nurses and childcare workers from the workforce.

* Characterizing the increased ratio of female success in the education as a
  victory, when the increase is actually due to an increasing rate of failure
  among men.

* Announcing that 'men can stop rape', characterising all men as willing
  accomplices to rape, if not active participants. (not to mention placing the
  blame on male victims of rape, and onto family, friends and loved ones of rape
  victims)

* Implementing Affirmative Action to enforce sexual discrimination as a matter
  of policy, while completely masking the underlying problems leading to a lack
  of women on track for the positions in question in the first place.

  It's shit like this that gives feminism a bad name, and leads people to oppose it.

* There's active feminist protesting and lobbying to prevent funding to male
  shelters and to deny that partner abuse of men by women is about as common as
  the reverse.

> Once girls were taught to be sweet, kind, demure, quiet, gentle, and pleasant.
> These are now seen not as virtues but as manifestations of oppression.
> Feminists have mistakenly concluded that men must possess the opposite of the
> feminine virtues, therefore men must be harsh, rude, loud, aggressive, nasty,
> and foul-mouthed, and therefore women who want power over men must become these
> things. It’s a lie of course, but that hasn’t stopped women in general from
> becoming progressively crasser and more ill-mannered.
> 
> <footer>-- *"[On the Uglification of Modern Women](#refs)"* Sunshine Mary and the Dragon (2013).</footer>


## Female Enslavement

Feminists contend that patriarchy and its "male enablers" enslave women through
marriage and motherhood. They assert that men are sex-driven, lustful pigs keen
on subduing women. As a result, feminists are determined to undermine them.

1. A typical feminist believes men enslave women through marriage:

   > "Since marriage constitutes slavery for women, it is clear that the Women’s
   > Movement must concentrate on attacking this institution. Freedom for women
   > cannot be won without the abolition of marriage." 
   > <footer>-- *"Radical Feminism - Marriage"* (Sheila Cronan, 1970)</footer>

2. A typical feminist thinks men are rapists:

   > Male sexual aggression is endemic, if any sex act against a person’s will
   > were considered rape, the majority of men would be rapists.
   > <footer>-- *"The War Against Women, Ballantine Books, 1992, p. 193"* (Marily French, 1992)</footer>

3. A typical feminist wants to eradicate manliness:

   > Only when manhood is dead–and it will perish when ravaged femininity no
   > longer sustains it–only then will we know what it is to be free."
   > <footer>-- *"Andrea Dworkin Our Blood: Prophecies And Discourses On Sexual Politics"*</footer>

## Feminism, Past and Future

Unfortunately, feminism and future is an oxymoron (or fortunately, depending on
your point-of-view), as it seems to be unsustainable on the long run.

Based on past history, it appears that a civilization that embraces feminist
values will cease to exist in just a few centuries. This is why we have never
seen a feminist civilization aside from very short spans at the end of the
Roman empire and possibly a few other more ancient civilizations.

Reading the history of the roman Empire brings such glaring similarities with
our own civilization, it is as if human social dynamics are literally stuck in
a cycle that repeats every couple thousand years (there were two matriarchical,
extremely advanced civilizations: one at the end of the Roman empire, 2000
years ago, one possibly at the end of Babylon, 4000 years ago).

For those who enjoy history, here is a short recap of social changes in Rome, 2
millenia ago (most historians focus on military and political facts, but I find
the social aspects just as fascinating):

* ~5 century BC: Roman civilization is a a strong patriarchy, fathers are
  liable for the actions of their wife and children, and have absolute
  authority over the family (including the power of life and death)

* ~1 century BC: Roman civilization blossoms into the most powerful and
  advanced civilization in the world. Material wealth is astounding, citizens
  (i.e.: non slaves) do not need to work. They have running water, baths and
  import spices from thousands of miles away. The Romans enjoy the arts and
  philosophy; they know and appreciate democracy, commerce, science, human
  rights, animal rights, children rights and women become emancipated. No-fault
  divorce is enacted, and quickly becomes popular by the end of the century.

* ~1-2 century AD: The family unit is destroyed. Men refuse to marry and the
  government tries to revive marriage with a "bachelor tax", to no avail.
  Children are growing up without fathers, Roman women show little interest in
  raising their own children and frequently use nannies. The wealth and power
  of women grows very fast, while men become increasingly demotivated and
  engage in prostitution and vice. Prostitution and homosexuality become
  widespread.

* ~3-4 century AD: A moral and demographic collapse takes place, Roman
  population declines due to below-replacement birth-rate. Vice and massive
  corruption are rampant, while the new-born Catholic Religion is gaining power
  (it becomes the religion of the Empire in 380 AD). There is extreme economic,
  political and military instability: there are 25 successive emperors in half
  a century (many end up assassinated), the Empire is ungovernable and on the
  brink of civil war.

* ~5 century AD: The Empire is ruled by an elite of military men that use the
  Emperor as a puppet; due to massive debts and financial problems, the Empire
  cannot afford to hire foreign mercenaries to defend itself (Roman citizens
  have long ago being replaced by mercenaries in the army), and starts
  "selling" parts of the Empire in exchange for protection. Eventually, the
  mercenaries figure out that the "Emperor has no clothes", and overrun and
  pillage the Empire.

Humanity falls back into the Bronze Age (think: eating squirrel meat and living
in a cave); 12 centuries of religious zilotry (The Great Inquisition, Crusades)
and intellectual darkness follow: science, commerce, philosophy, human rights
become unknown concepts until they are rediscovered again during the Age of
Enlightenment in 17th century AD.

Regarding the Babylonian civilization (~2,000 BC), we have relatively few
records, but we do know that they had a very advanced civilization because we
found their legislative code written down on stone tablets (yes, they had laws
and tribunals, and some of today's commercial code can even be traced back to
Babylonian law). They had child support laws (which seems to indicate that
there was a family breakdown), and they collapsed presumably due to a "moral
breakdown" figuratively represented in the Bible as the "Tower of Babel" (which
was inspired by a real tower). Interesting and controversial anecdote: some
claim that the Roman Catholic Religion is nothing more than a rewriting and
adaptation of an ancient Babylonian religion!

[[!meta updated="Thu Jun  6 10:18:34 PDT 2013"]]
[[!meta title="Feminism"]]
[[!meta author="Defiance"]]
[[!meta authorurl="http://point-defiance.rhcloud.com/Authors/Defiance"]]
[[!meta license="This work is [[free|PrimarilyPublicDomain]] of known copyright restrictions."]]
[[!meta copyright="&copy; 2013 by Defiance"]]
[[!tag MensIssues Reference Feminism History Popular]]
[[!inline pages="Men/CommonReferences" feeds="no" raw="yes"]]
