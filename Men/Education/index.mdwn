Starting in early childhood, men are not encouraged to do well in school.
Every aspect of common-knowledge in today's society regarding rasing boys seems
to center around ensuring that they will be subservient to the system in which
they live.  In public education, this is easy to demonstrate.  Boys are ignored
and rarely encouraged compared to their female countparts.  While little girls
are told to believe they can achieve anything they can dream (girl power!),
little boys watch and are ignored.

This setup continues into middle and high school, and once compusulary
education ends - a growing gap is seen in higher education.  40% of college
atendees are men, and xx% are college graduates.  Men see little point in
completing an education when it becomes evident that the deck is stacked
against them.

> David Benatar, head of philosophy at the University of Cape Town. He's even
> written a book on the subject, entitled The Second Sexism. In an interview
> with the BBC last year, Mr Benatar referred to recent figures on education as
> proof of his theory. Tests in 2009 by the Programme for International Student
> Assessment showed that boys lagged a year behind girls at reading in every
> industrialised country. And women now make up the majority of
> undergraduates.  "When women are underrepresented as CEOs of companies that is
> deemed discrimination," he bleats, "But when boys are falling behind at
> school, when 90% of people in prison are male, there's never any thought
> given to whether men are discriminated against."

-------------------------------------------------------------------------------

In [White Dudes need not Apply][ew1], an example of systematic scholarship and
admissions denial to american universities is explored.

-------------------------------------------------------------------------------

Cover the over-prescribing of harmful stimulants [[Ritalin]], Adderall, etc. almost
exlusively to boys and almost never to girls (please see [[Depression]] &
[[Suicide]])....but we won't.

[[!meta title="Education"]]
[[!meta author="Defiance"]]
[[!meta authorurl="http://point-defiance.rhcloud.com/Authors/Defiance"]]
[[!meta license="This work is [[free|PrimarilyPublicDomain]] of known copyright restrictions."]]
[[!meta copyright="&copy; 2013 by Defiance"]]
[[!tag Inequality MensIssues Reference SocialHierarchy Education Boys Fatherhood]]
[[!inline pages="Men/CommonReferences" feeds="no" raw="yes"]]
