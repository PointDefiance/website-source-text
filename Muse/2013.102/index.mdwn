### The Gender Neutral Language Bill
The [[TNT]] reports that the Washington State House passed a gender-neutral
[language bill][1].  This is just stupid.  Talking about a waste of taxpayer's
dollars.

### Wayward Sons

[Wayward Sons: The Emerging Gender Gap in Labor Markets and Education][2] is a
new report which shows that girls and young women outperform boys and young men
in both education and key aspects of the workforce.

Of course, the issue was covered in-depth by Dalrock in [Why aren’t men responding to economic signals?][3].

[1]: http://www.thenewstribune.com/2013/04/09/2549763/wash-house-passes-gender-neutral.html
    "Tacoma News Tribune: Washington State House Passes Gender Neutrality Bill"
[2]: http://www.thirdway.org/publications/662
    "Third Way Report: The Emerging Gender Gap in Labor Markets and Education"
[3]: https://dalrock.wordpress.com/2013/03/29/why-arent-men-responding-to-economic-signals/
    "Dalrock: Why Arent Men Responding to Economic Signals"

[[!meta date="Fri Apr 12 00:00:00 PDT 2013"]]
[[!meta title="Muse for Friday, April 12 2013"]]
[[!meta author="Defiance"]]
[[!meta authorurl="http://point-defiance.rhcloud.com/Authors/Defiance"]]
[[!meta copyright="&copy; 2013 by Defiance, All rights reserved."]]
[[!tag Muse Dumb Politics/Local WashingtonState ChildCustody Fatherhood MensIssues Studies Survey TNT Dalrock]]
[[!inline pages="Muse/CommonReferences" feeds="no" raw="yes"]]
