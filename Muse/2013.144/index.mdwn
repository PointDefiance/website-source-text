### Suicide is now the leading cause of death

at least for people between the ages of 15 and 49.  4 out of 5 of those
suicides are men.  The Daily Beast has [lengthy article discussing suicide][1],
the research done about the subject, and the numbers.

> Throughout the developed world, for example, self-harm is now the leading
> cause of death for people 15 to 49, surpassing all cancers and heart disease.
> That’s a dizzying change, a milestone that shows just how effective we are at
> fighting disease, and just how haunted we remain at the same time. Around the
> world, in 2010 self-harm took more lives than war, murder, and natural
> disasters combined, stealing more than 36 million years of healthy life
> across all ages. In more advanced countries, only three diseases on the
> planet do more harm.

[1]: http://www.thedailybeast.com/newsweek/2013/05/22/why-suicide-has-become-and-epidemic-and-what-we-can-do-to-help.html
    "The Daily Beast: Suicide has become Epidemic"

[[!meta date="Fri May 24 10:17:42 PDT 2013"]]
[[!meta title="Muse for Friday, May 24 2013"]]
[[!meta author="Defiance"]]
[[!meta authorurl="http://point-defiance.rhcloud.com/Authors/Defiance"]]
[[!meta copyright="&copy; 2013 by Defiance, All rights reserved."]]
[[!tag Muse MensIssues Suicide MensHealth ]]
[[!inline pages="Muse/CommonReferences" feeds="no" raw="yes"]]
