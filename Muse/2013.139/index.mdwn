### If Women Ruled

It's an accepted truth among [[Feminists|Men/Feminism]] that if women 
"[[ruled the world|Muse/2013.134]]" there would be far less war and strife.
This is usually attributed to women's more nurturing nature.

An article [posted by the Native Canadian][1] today lists some famous women
throughout the ages, many of whom are poster-book examples of early feminine
power.  He then comments:

> Its a myth that women possess an inborn trait that makes them 'good people',
> simply because 'good' is a matter of character, not gender.

and proceeds to single out a few from the above list gave examples of their
leadership, beginning with:

> Irene of Athens (8th century): had her son blinded so she could become
> empress of the Byzantine empire. 

I think that women are as cruel as men, and can far worse if their emotions get
involved.  We should look for leaders who have character and exhibit traits we
would like to emulate.  In both men and women.

### Gender Discrimination Chart

[[!img GenderChart.png
alt="Facts about Gender Discrimination"
title="Facts about Gender Discrimination"
size="550x"
class="aligncenter article-image"
]]

[1]: http://nativecanadian.blogspot.ca/2013/05/exposing-more-feminist-myths-women-are.html
    "The Native Canadians: Exposing more feminist myths, women are better leaders"

[[!meta date="Sun May 19 10:01:13 PDT 2013"]]
[[!meta title="Muse for Sunday, May 19 2013"]]
[[!meta author="Defiance"]]
[[!meta authorurl="http://point-defiance.rhcloud.com/Authors/Defiance"]]
[[!meta copyright="&copy; 2013 by Defiance, All rights reserved."]]
[[!tag Muse Feminism Leadership MensIssues Discrimination Chart]]
[[!inline pages="Muse/CommonReferences" feeds="no" raw="yes"]]
