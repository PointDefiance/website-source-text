### Comparing Marriage version 1 and 2

There is a [neat chart][1] posted over at [[BlogRoll/SocietyofPhineas]]'s
website comparing what [[BlogRoll/Dalrock]] and the [[Men/TradCons]] call
[[Marriage 2.0|Men/Marriage2]] and a more traditionalist marriage.  

It's interesting in that it really looks at how marriage is viewed by the
more traditionalist christian crowd when they are speaking to modern
christians.  The gap between modern marriage and "Marriage 2.0" is even wider
if you look at how marriage is looked upon by the secularists.

It also shows how marriage has evolved as a three party covenant between
Man-Woman-God to a pact between Man-Woman-State.

[1]: http://societyofphineas.wordpress.com/2013/05/24/marriage-1-0-vs-marriage-2-0/
    "Society of Phineas: Marriage 1.0 vs Marriage 2.0"

[[!meta updated="Sat Jun  1 10:20:16 PDT 2013"]]
[[!meta date="Sat Jun  1 10:14:49 PDT 2013"]]
[[!meta title="Muse for Saturday, June 01 2013"]]
[[!meta author="Defiance"]]
[[!meta authorurl="http://point-defiance.rhcloud.com/Authors/Defiance"]]
[[!meta license="This work is [[free|PrimarilyPublicDomain]] of known copyright restrictions."]]
[[!meta copyright="&copy; 2013 by Defiance, All rights reserved."]]
[[!tag Muse Marriage SocietyofPhineas Tradcons Religion]]
[[!inline pages="Muse/CommonReferences" feeds="no" raw="yes"]]
