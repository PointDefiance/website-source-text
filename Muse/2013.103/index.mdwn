### Manosphere Humor

[[BlogRoll/IanIronwood]] of [The Red Pill Room][1] posted a [somewhat humorous
article describing female behavior][2] in the business world.  From what I
remember, it explains quite a bit.

### Parade (not)

So I got up this morning and walked down to Pacific Ave to grab some breakfast
and maybe watch the [Daffodil Parade][3].  Saw a bunch of (former) daffodil
princesses and their jock boyfriends, and left.  I get enough drama in my day,
no need for more of it

[1]: http://theredpillroom.blogspot.com
    "Ian Ironwood"
[2]: http://theredpillroom.blogspot.com/2013/04/the-crab-basket-effect.html
    "The Red Pill Room"
[3]: http://thedaffodilfestival.org/
    "Daffodil Parade"

[[!meta updated="Tue Jun 11 13:37:47 PDT 2013"]]
[[!meta date="Sat Apr 13 13:11:00 PDT 2013"]]
[[!meta title="Muse for Saturday, April 13 2013"]]
[[!meta author="Defiance"]]
[[!meta authorurl="http://point-defiance.rhcloud.com/Author/Defiance"]]
[[!meta copyright="&copy; 2013 by Defiance, All rights reserved."]]
[[!tag Muse Behavior Business Humor MensIssues QueenBee SocialHierarchy RedPillRoom Tacoma Events]]
[[!inline pages="Muse/CommonReferences" feeds="no" raw="yes"]]
