### More Minorities, More Unemployment

In [More minorities, more unemployment][1] [[BlogRoll/VoxDay]] tackles a
hot-potato.  His assertion is that the "white Protestant Americans" have an
established work ethic, and to reduce that population will result in a loss in
economic terms.  Not very politically correct, but interesting.

### The Military Tuck

[Use a Military Tuck to Keep Dress Shirts from Billowing at the Waist][2] would
work great if I wasn't ... large about the waist.

[1]: http://voxday.blogspot.com/2013/04/more-minorities-more-unemployment.html
    "Vox Day: More minorities, more unemployment"
[2]: http://lifehacker.com/5994069/use-a-military-tuck-to-keep-dress-shirts-from-billowing-at-the-waist
    "Lifehacker: Use a Military Tuck to Keep Dress Shirts from Billowing at the Waist"

[[!meta updated="Tue Jun 11 13:36:35 PDT 2013"]]
[[!meta date="Thu Apr 11 21:14:00 PDT 2013"]]
[[!meta title="Muse for Thursday, April 11 2013"]]
[[!meta author="Defiance"]]
[[!meta authorurl="http://point-defiance.rhcloud.com/Authors/Defiance"]]
[[!meta copyright="&copy; 2013 by Defiance, All rights reserved."]]
[[!tag Muse Economics Politics/InCorrect SocialJustice Unemployment VoxDay Dress Tips Lifehacker]]
[[!inline pages="Muse/CommonReferences" feeds="no" raw="yes"]]
