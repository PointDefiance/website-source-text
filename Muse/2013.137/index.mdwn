### Judge Training
[[!img CSPIA.Calc.jpg
alt="Proof that Mom is the better Parent"
title="Proof that Mom is the better Parent"
caption="Cartoon created by Gary Tatum"
size="200x"
class="alignright article-image"
]]

The states get reimbursed by the Federal government depending on their
enforcement performance and the amount of money they collect from non custodial
parents.  It's not right.  Found this neat little cartoon on reddit and
reproduced it here.

### The New Math on Campus

In "[The New Math on Campus][2]", female students bemoan the lack of men on
campus to date.

> Jayne Dallas, a senior studying advertising who was seated across the table,
> grumbled that the population of male undergraduates was even smaller when you
> looked at it as a dating pool. “Out of that 40 percent, there are maybe 20
> percent that we would consider, and out of those 20, 10 have girlfriends, so
> all the girls are fighting over that other 10 percent,” she said.

Talk about a perfect example of Hypergamy in action.  The single women in
college are chasing the top 10% of the men.  Screw the rest.

### Filtching Abercrombie

When Abercrombie CEO Mike Jeffries announced their "no women's clothing above a
size 10" policy it created a riot of indignation.  A Los-Angeles writer decided
that the [right response][1] would be to give all his stylish clothes away to
the Homeless.  His idea was to use the homeless who are seen as dirty,


[1]: http://www.vice.com/read/fitch-the-homeless-is-backwards-ass-activism
    "Vice.com: Fitch the Homeless is Backwards-ass activism"
[2]: http://www.nytimes.com/2010/02/07/fashion/07campus.html?_r=0&pagewanted=all
    "New York Times: The New Math on Campus"

[[!meta date="Fri May 17 09:17:19 PDT 2013"]]
[[!meta title="Muse for Friday, May 17 2013"]]
[[!meta author="Defiance"]]
[[!meta authorurl="http://point-defiance.rhcloud.com/Authors/Defiance"]]
[[!meta copyright="&copy; 2013 by Defiance, All rights reserved."]]
[[!tag Muse Homeless Activism Prejudice Hypergamy HookupCulture ChildSupport]]
[[!inline pages="Muse/CommonReferences" feeds="no" raw="yes"]]
