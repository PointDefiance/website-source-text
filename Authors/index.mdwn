The Following are Authors and Regular Contributors to this site.  Feel free to
add your own page, hype your blog, etc.

[[!inline 
   pages="Authors/* and !Authors/CommonReferences and !*.png and !*.jpg and !*/pingback* and !*/refmeta* and !*/nearlink*" 
   archive=yes
   feeds=no
   feedshow=0
   template="articlelist"
   description="Point Defiance Authors"
]]

[[!meta date="April 05, 2013"]]
[[!meta title="Authors and Contributors"]]
[[!meta author="Defiance"]]
[[!meta authorurl="http://point-defiance.rhcloud.com/Authors/Defiance"]]
[[!meta license="This work is [[free|PrimarilyPublicDomain]] of known copyright restrictions."]]
[[!meta copyright="&copy; 2013 by Defiance"]]
[[!inline pages="/SiteReferences" feeds="no" raw="yes"]]

